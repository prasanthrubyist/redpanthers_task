require 'spec_helper'
require 'format_csv_exporter.rb'

describe CsvExporter do
  let(:csv_exporter) { CsvExporter.new }

  describe '#export' do
    let(:resume) { "name,Prasanth\nage,23\ndegree,B.Tech\ncompany, Egrove systems" }

    after(:all) do
      file_array = Dir['resume_*.*']
      file_array.each { |f| File.delete(f) }
    end

    it 'Create resume in csv format' do
      csv_exporter.export resume
      expect(Dir['resume_*.csv'].empty?).to eq(false)
    end
  end

  describe '#self.respond_to(file_type)' do
    it 'It will returns true if input is csv' do
      expect(CsvExporter.respond_to('csv')).to eq(true)
    end

    it 'It will returns false if input is txt' do
      expect(CsvExporter.respond_to('txt')).to eq(false)
    end
  end
end
