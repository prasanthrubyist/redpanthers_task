require 'spec_helper'
require 'employee.rb'

RSpec.describe Employee do

  let(:dummy) do
    {
      name: 'Prasanth',
      age: 23,
      degree: 'B.Tech',
      company: 'Egrove systems'
    }
  end

  let(:dummy_emp) { Employee.new(dummy[:name], dummy[:age], dummy[:degree], dummy[:company]) }
  let(:expected_output) { "name,#{dummy[:name]}\nage,#{dummy[:age]}\ndegree,#{dummy[:degree]}\ncompany,#{dummy[:company]}" }

  describe 'instantiation' do
    it 'Put the name' do
      expect(dummy_emp.name).to eq(dummy[:name])
    end

    it 'Put the age' do
      expect(dummy_emp.age).to eq(dummy[:age])
    end
    it 'Put the degree' do
      expect(dummy_emp.degree).to eq(dummy[:degree])
    end
    it 'Put the company' do
      expect(dummy_emp.company).to eq(dummy[:company])
    end
  end

  describe '#create_resume' do
    it 'creates resume in string format' do
      expect(dummy_emp.create_resume).to eq(expected_output)
    end
  end
end
