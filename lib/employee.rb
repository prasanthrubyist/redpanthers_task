class Employee
  attr_accessor :name, :age, :degree, :company

  def initialize(name, age, degree, company)
    @name = name
    @age = age
    @degree = degree
    @company = company
  end

  def create_resume
    "name,#{@name}\nage,#{@age}\ndegree,#{@degree}\ncompany,#{@company}"
  end
end